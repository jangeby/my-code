<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Looping PHP</title>
</head>
<body>
        <h1>Looping PHP</h1>

    <?php
        echo    "<h3>Soal No 3 Looping</h3>";

            echo "<h5>Looping Pertama</h5>";
            for($i=2;$i <= 20; $i++){ // atau bisa mengunakan i+=1/2,  jika mengunakan i=+2 maka kelipatan 2
                echo $i. "- ILY PHP<br>";
            }

            echo  "<h3>Soal Asterix</h3>";
             $star=5; // Contoh Test Lamaran Kerja 
             for($a=$star; $a >  0; $a--){
                for($b=$star; $b >= $a; $b--){
                    echo "*";
                }
                echo "<br>";
             }
             echo "<br>";
             echo "<br>";
             for ($s=1; $s<=5; $s++){
                 for($b=1;$b<=$s;$b++){
                     echo "*";
                 }
                 echo "<br>";
             }
    ?>

</body>
</html>